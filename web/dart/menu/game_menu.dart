part of game;

class GameMenu extends Menu {

  String levelfile;
  var level;
  var message;

  GameMenu(this.levelfile): super();

  @override
  void create() {
    buildFromTemplate(
        assetManager.get('html/hud.html'), {'health': 100, 'mana': 100});
  }

  @override
  show() {
    super.show();
    message = querySelector('.message');
    new Timer(new Duration(milliseconds: 4), showMessage);
    new Timer(new Duration(seconds: 5), hideMessage);
  }

  void showMessage() {
    message.style.transform = 'translate(0%, 0%)';
  }

  void hideMessage() {
    message.style.transform = 'translate(0%, -200%)';
  }

  @override
  preload() {
    assetManager.load(
        'html/hud.html', HttpRequest.getString('html/hud.html'));
  }

  void update(var data, var level) {
    querySelector('.health').style.height = 'calc(' + data['health'].toString() + '% - 1em)';
    querySelector('.mana').style.height = 'calc(' + data['mana'].toString() + '% - 1em)';
    message.text = level['title'];
  }

  void death(var level) {
    querySelector('h2.death').text = level['death'];
    querySelector('.deathoverlay').style.transform = 'translate(-50%, -50%)';
    querySelector('.deathoverlay').style.top = '50%';
  }

  void victory(var level) {
    querySelector('h2.victory').text = level['victory'];
    querySelector('.victoryoverlay').style.transform = 'translate(-50%, -50%)';
    querySelector('.victoryoverlay').style.top = '50%';
    if(level['final']) {
      querySelector('#continue').hidden = true;
      querySelector('#mainmenu').hidden = false;
    }
    this.level = level;
  }

  @override
  void bind() {
    subscribe(querySelector('#retry').onClick.listen(restart));
    subscribe(querySelector('#continue').onClick.listen(next));
    subscribe(querySelector('#mainmenu').onClick.listen(mainmenu));
  }

  restart(e) {
    game.pushState(new GameState(levelfile));
  }

  next(e) {
    game.pushState(new GameState(level['next']));
  }

  mainmenu(e) {
    game.pushState(new MainMenuState());
  }

}