part of game;

class MainMenu extends Menu {

  @override
  void bind() {
    subscribe(querySelector('#start').onClick.listen(startGame));
  }

  @override
  void create() {
    buildFromTemplate(
        assetManager.get('html/main.html'), {'name': 'LD39 Game'});
  }

  @override
  preload() {
    assetManager.load(
        'html/main.html', HttpRequest.getString('html/main.html'));
  }

  void startGame(e) {
    game.pushState(new GameState('level1.yml'));
  }

  @override
  show() {
    super.show();
    assetManager.get('menu').loop();
  }

  @override
  hide() {
    super.hide();
    assetManager.get('menu').stop();
  }

}

class MainMenuState extends MenuState {

  World world;

  MainMenuState() : super(new MainMenu()) {

  }

  @override
  create() {
    super.create();

    world = new World();

    createProp(world, atlas['background1'], 8.0 * -9.5, -8.0);

    Entity player = world.createEntity();
    player.addComponent(new Position(0.0, 0.0));
    player.addComponent(new Animation([atlas['player1'], atlas['player2']]));
    player.addToWorld();

    world.addSystem(new AnimationSystem());
    world.addSystem(new RenderSystem());

    world.initialize();
  }

  @override
  render(num delta) {
    clearScreen(95 / 255, 205 / 255, 228 / 255, 1.0);

    world.delta = delta;
    world.process();
  }

  @override
  resize(num width, num height) {
    RenderSystem system = world.getSystem(RenderSystem);
    system.resize(width, height);
  }

  @override
  update(num delta) {

  }

}