part of game;

class AllUriPolicy implements UriPolicy {
  bool allowsUri(uri) => true;
}

abstract class Menu {

  Element ui;

  List<StreamSubscription<dynamic>> subscriptions;

  String html;

  Menu() {
    ui = querySelector('#ui');
    subscriptions = [];
  }

  void preload() {}

  void buildFromHtml(String html) {
    this.html = html;
  }

  void buildFromTemplate(String template, data) {
    html = Mustache.parse(template).renderString(data);
  }

  void create();

  void subscribe(StreamSubscription subscription) {
    subscriptions.add(subscription);
  }

  void bind();

  void unbind() {
    for (StreamSubscription subscription in subscriptions) {
      subscription.cancel();
    }
    subscriptions.clear();
  }

  void show() {
    querySelector('#ui').setInnerHtml(html, validator: new NodeValidatorBuilder.common()
      ..allowElement('a', attributes: ['href'], uriPolicy: new AllUriPolicy())
      ..allowInlineStyles());
    bind();
  }

  void hide() {
    querySelector('#ui').setInnerHtml('');
    unbind();
  }

}

abstract class MenuState extends State {

  Menu menu;

  MenuState(this.menu);

  @override
  create() {
    menu.create();
    menu.show();
  }

  @override
  preload() {
    menu.preload();
  }

  @override
  pause() {
    menu.unbind();
    menu.hide();
  }

  @override
  resume() {
    menu.bind();
  }

}
