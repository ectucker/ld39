part of game;

class SpawnSystem extends VoidEntitySystem {

  List spawns;
  double time = 0.0;

  SpawnSystem(var level) : super() {
    this.spawns = [];
    for(var spawn in level['spawns']) {
      spawns.add(new Map.from(spawn));
    }
  }

  @override
  void processSystem() {
    time += world.delta;
    for(var spawn in spawns) {
      if(spawn['time'] <= time && !spawn['done']) {
        if(spawn['type'] == 'grunt') {
          createGrunt(world, spawn['x']);
        } else if(spawn['type'] == 'firemole') {
          createHellMole(world, spawn['x']);
        } else if(spawn['type'] == 'bombmole') {
          createBombMole(world, spawn['x']);
        } else if(spawn['type'] == 'flower') {
          createFlower(world, spawn['x']);
        } else if(spawn['type'] == 'arborist') {
          createArborist(world, spawn['x']);
        } else if(spawn['type'] == 'iceblob') {
          createIceBlob(world, spawn['x']);
        } else if(spawn['type'] == 'hellhound') {
          createHellhound(world, spawn['x']);
        } else if(spawn['type'] == 'lavabeast') {
          createLavaBeast(world, spawn['x']);
        } else if(spawn['type'] == 'frozenman') {
          createFrozenMan(world, spawn['x']);
        }
        spawn['done'] = true;
      }
    }
  }

}