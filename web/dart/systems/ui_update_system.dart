part of game;

class UIUpdateSystem extends VoidEntitySystem {

  GameMenu menu;
  Entity player;

  Mapper<Health> healthMapper;
  Mapper<PlayerSpell> spellMapper;

  var level;

  UIUpdateSystem(this.player, this.menu, this.level) : super();

  @override
  void initialize() {
    healthMapper = new Mapper<Health>(Health, world);
    spellMapper = new Mapper<PlayerSpell>(PlayerSpell, world);
  }

  @override
  void processSystem() {
    menu.update({'health': healthMapper[player].amount, 'mana': spellMapper[player].mana}, level);
  }

}