part of game;

class PlayerSpellSystem extends EntityProcessingSystem {

  Mapper<Position> positionMapper;
  Mapper<PlayerSpell> playerSpellMapper;
  Mapper<Input> inputMapper;
  Mapper<Flip> flipMapper;
  Mapper<PlayerSound> soundMapper;

  PlayerSpellSystem() : super(Aspect.getAspectForAllOf([Flip, Position, PlayerSpell, Input])) {}

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    playerSpellMapper = new Mapper<PlayerSpell>(PlayerSpell, world);
    inputMapper = new Mapper<Input>(Input, world);
    flipMapper = new Mapper<Flip>(Flip, world);
    soundMapper = new Mapper<PlayerSound>(PlayerSound, world);
  }

  @override
  void processEntity(Entity entity) {
    Input input = inputMapper[entity];
    PlayerSpell spell = playerSpellMapper[entity];
    Position position = positionMapper[entity];
    Flip flip = flipMapper[entity];

    spell.fireCooldown -= world.delta;
    if(input.castSpell1 && spell.fireCooldown <= 0.0 && spell.mana > 1) {
      if(flip.x) {
        for(int i = 0; position.x - i * 8 > -20 * 8; i++) {
          createFireBeam(world,  position.x - 8 - i * 8, position.y + 4);
        }
      } else {
        for(int i = 0; position.x + i * 8 < 20 * 8; i++) {
          createFireBeam(world, position.x + 8 + i * 8, position.y + 4);
        }
      }
      spell.fireCooldown = 0.1;
      spell.mana -= 1;
      soundMapper[entity].fireBeam.play();
    }

    spell.iceCooldown -= world.delta;
    if(input.castSpell2 && spell.iceCooldown <= 0.0 && spell.mana > 10) {
      if(flip.x) {
        createIceCone(world, position.x - 8, position.y, flip.x);
      } else {
        createIceCone(world, position.x + 8, position.y, flip.x);
      }
      spell.mana -= 5;
      spell.iceCooldown = 0.2;
      soundMapper[entity].iceShot.play();
    }

    spell.earthCooldown -= world.delta;
    if(keyboard.keyPressed(KeyCode.C) && spell.earthCooldown <= 0.0 && spell.mana > 20) {
      if(flip.x) {
        createDropIndicator(world, position.x - 32.0);
      } else {
        createDropIndicator(world, position.x + 32.0);
      }
      spell.earthCooldown = 0.1;
    }
    if(input.castSpell3 && spell.mana > 20) {
      if(flip.x) {
        createEarthShell(world, position.x - 32.0);
      } else {
        createEarthShell(world, position.x + 32.0);
      }
      spell.mana -= 13;
      spell.earthCooldown = 0.3;
      soundMapper[entity].earthMortar.play();
    }
  }
}