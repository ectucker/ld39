part of game;

class MovementSystem extends EntityProcessingSystem {

  Mapper<Position> positionMapper;
  Mapper<Velocity> velocityMapper;

  MovementSystem() : super(Aspect.getAspectForAllOf([Position, Velocity])) {}

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    velocityMapper = new Mapper<Velocity>(Velocity, world);
  }

  @override
  void processEntity(Entity entity) {
    Position position = positionMapper[entity];
    Velocity velocity = velocityMapper[entity];

    position.vec += velocity.vec * world.delta;
  }

}