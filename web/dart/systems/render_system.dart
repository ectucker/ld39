part of game;

class RenderSystem extends EntityProcessingSystem {

  static const int tileSize = 8;
  static const int tilesX = 20;

  int pixelWidth;
  int pixelHeight;

  double tileScreenSize;
  double tilesY;

  SpriteBatch batch;
  PhysboxBatch debugBatch;
  List<Aabb2> renderLater = [];

  Camera2D fullscreen;

  Mapper<Position> positionMapper;
  Mapper<StaticTexture> textureMapper;
  Mapper<Animation> animationMapper;
  Mapper<Hitbox> hitboxMapper;
  Mapper<Flip> flipMapper;

  RenderSystem() : super(Aspect.getAspectForAllOf([Position]).oneOf([StaticTexture, Animation, Hitbox])) {
    batch = new SpriteBatch.defaultShader();
    //debugBatch = new PhysboxBatch.defaultShader();
    resize(width, height);
  }

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    textureMapper = new Mapper<StaticTexture>(StaticTexture, world);
    animationMapper = new Mapper<Animation>(Animation, world);
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
    flipMapper = new Mapper<Flip>(Flip, world);
  }

  @override
  void begin() {
    camera.update();
    fullscreen.update();

    setGLViewport(width, height);
    //setGLViewport(pixelWidth, pixelHeight);
    //pixel.beginCapture();

    batch.projection = camera.combined;
    batch.begin();
  }

  @override
  void processEntity(Entity entity) {
    Position position = positionMapper[entity];

    bool flipX, flipY = false;
    if(flipMapper.has(entity)) {
      flipX = flipMapper[entity].x;
      flipY = flipMapper[entity].y;
    }

    if(textureMapper.has(entity)) {
      batch.draw(textureMapper[entity].texture, position.x, position.y, flipX: flipX, flipY: flipY);
    }
    if(animationMapper.has(entity)) {
      batch.draw(animationMapper[entity].frame, position.x, position.y, flipX: flipX, flipY: flipY);
    }
    //if(hitboxMapper.has(entity)) {
    //  renderLater.add(hitboxMapper[entity].box);
    //}
  }

  @override
  void end() {
    batch.end();

    //debugBatch.projection = camera.combined;
    //debugBatch.begin();
    //for(Aabb2 box in renderLater) {
    //  debugBatch.draw2D(box);
    //}
    //renderLater.clear();
    //debugBatch.end();

    //pixel.endCapture();

    setGLViewport(width, height);
    //pixel.render(fullscreen.combined, 0, 0, width, height);
  }

  resize(int width, int height) {
    tileScreenSize = width / tilesX;
    tilesY = height / tileScreenSize;

    pixelWidth = tilesX * tileSize;
    pixelHeight = (tilesY * tileSize).ceil();

    fullscreen = new Camera2D.originBottomLeft(width, height);
    camera = new Camera2D.originBottomLeft(width, height);
    camera.setScale(width / pixelWidth, width / pixelWidth);
    camera.setTranslation(tileSize * 9.5 * camera.scaleX, tileSize * camera.scaleX);
  }

}