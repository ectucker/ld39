part of game;

class HitboxPositionSystem extends EntityProcessingSystem {

  Mapper<Position> positionMapper;
  Mapper<Hitbox> hitboxMapper;

  HitboxPositionSystem() : super(Aspect.getAspectForAllOf([Position, Hitbox])) {}

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
  }

  @override
  void processEntity(Entity entity) {
    Position position = positionMapper[entity];
    Hitbox hitbox = hitboxMapper[entity];

    hitbox.box.setCenterAndHalfExtents(
        new Vector2(position.x + hitbox.width / 2 + hitbox.offsetX, position.y + hitbox.height / 2 + hitbox.offsetY),
        new Vector2(hitbox.width / 2, hitbox.height / 2));
  }

}