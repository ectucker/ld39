part of game;

class GruntAISystem extends EntityProcessingSystem {

  Mapper<Position> positionMapper;
  Mapper<Input> inputMapper;
  Mapper<GruntAI> aiMapper;
  Mapper<EntitySound> soundMapper;

  Entity player;

  GruntAISystem(this.player) : super(Aspect.getAspectForAllOf([Position, GruntAI, Input])) {}

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    inputMapper = new Mapper<Input>(Input, world);
    aiMapper = new Mapper<GruntAI>(GruntAI, world);
    soundMapper = new Mapper<EntitySound>(EntitySound, world);
  }

  @override
  void processEntity(Entity entity) {
    Input input = inputMapper[entity];
    Position position = positionMapper[entity];
    Position playerPosition = positionMapper[player];
    GruntAI ai = aiMapper[entity];

    ai.spellCooldown -= world.delta;

    input.moveLeft = false;
    input.moveRight = false;
    if(((playerPosition.x + 4) - (position.x + 4)).abs() > 32) {
      if(playerPosition.x < position.x) {
        input.moveLeft = true;
      } else if(playerPosition.x > position.x) {
        input.moveRight = true;
      }
    } else {
      if(playerPosition.x < position.x && ai.spellCooldown <= 0) {
        createBallOfLight(world, position.x - 4, true);
        ai.spellCooldown = 1.0;
        soundMapper[entity].fire.play();
      } else if(playerPosition.x > position.x && ai.spellCooldown <= 0) {
        createBallOfLight(world, position.x + 8, false);
        ai.spellCooldown = 1.0;
        soundMapper[entity].fire.play();
      }
    }

  }

}

class FlowerAISystem extends EntityProcessingSystem {

  Mapper<Position> positionMapper;
  Mapper<Input> inputMapper;
  Mapper<FlowerAI> aiMapper;
  Mapper<EntitySound> soundMapper;

  Entity player;

  FlowerAISystem(this.player) : super(Aspect.getAspectForAllOf([Position, FlowerAI, Input])) {}

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    inputMapper = new Mapper<Input>(Input, world);
    aiMapper = new Mapper<FlowerAI>(FlowerAI, world);
    soundMapper = new Mapper<EntitySound>(EntitySound, world);
  }

  @override
  void processEntity(Entity entity) {
    Input input = inputMapper[entity];
    Position position = positionMapper[entity];
    Position playerPosition = positionMapper[player];
    FlowerAI ai = aiMapper[entity];

    ai.spellCooldown -= world.delta;

    input.moveLeft = false;
    input.moveRight = false;
    if(((playerPosition.x + 4) - (position.x + 4)).abs() > 48) {
      if(playerPosition.x < position.x) {
        input.moveLeft = true;
      } else if(playerPosition.x > position.x) {
        input.moveRight = true;
      }
    } else {
      if(playerPosition.x < position.x && ai.spellCooldown <= 0) {
        createSeed(world, position.x - 4, true);
        ai.spellCooldown = 1.0;
        soundMapper[entity].fire.play();
      } else if(playerPosition.x > position.x && ai.spellCooldown <= 0) {
        createSeed(world, position.x + 8, false);
        ai.spellCooldown = 1.0;
        soundMapper[entity].fire.play();
      }
    }

  }

}