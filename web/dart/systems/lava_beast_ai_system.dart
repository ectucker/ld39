part of game;

class LavaBeastAISystem extends EntityProcessingSystem {

  Mapper<Position> positionMapper;
  Mapper<Input> inputMapper;
  Mapper<LavaBeastAI> aiMapper;
  Mapper<EntitySound> soundMapper;

  Entity player;

  LavaBeastAISystem(this.player) : super(Aspect.getAspectForAllOf([Position, LavaBeastAI])) {}

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    inputMapper = new Mapper<Input>(Input, world);
    aiMapper = new Mapper<LavaBeastAI>(LavaBeastAI, world);
    soundMapper = new Mapper<EntitySound>(EntitySound, world);
  }

  @override
  void processEntity(Entity entity) {
    Position position = positionMapper[entity];
    Position playerPosition = positionMapper[player];
    LavaBeastAI ai = aiMapper[entity];

    ai.spellCooldown -= world.delta;

    if(inputMapper.has(entity)) {
      Input input = inputMapper[entity];
      input.moveLeft = false;
      input.moveRight = false;
      if (((playerPosition.x + 4) - (position.x + 8)).abs() > 40 && ai.spellCooldown <= 2.0) {
        if (playerPosition.x < position.x) {
          input.moveLeft = true;
        } else if (playerPosition.x > position.x) {
          input.moveRight = true;
        }
      } else {
        if (playerPosition.x < position.x && ai.spellCooldown <= 0) {
          createLavaSpit(world, position.x - 1, true);
          ai.spellCooldown = 4.0;
        } else if (playerPosition.x > position.x && ai.spellCooldown <= 0) {
          createLavaSpit(world, position.x + 12, false);
          ai.spellCooldown = 4.0;
        }
        soundMapper[entity].fire.play();
      }
    } else if(position.y >= 0.0) {
      entity.addComponent(new Input());
      entity.changedInWorld();
    }
  }

}