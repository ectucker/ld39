part of game;

class VictorySystem extends EntitySystem {

  GameMenu menu;
  var level;

  double minTime = 0.0;
  double elapsedTime = 0.0;

  VictorySystem(this.menu, this.level) : super(Aspect.getAspectForOneOf([GruntAI, MoleAI, Damage, ArboristAI, BombMoleAI,
    FlowerAI, BlobAI, LavaBeastAI, FrozenManAI])) {
    for(var spawn in level['spawns']) {
      if(spawn['time'] > minTime) {
        minTime = spawn['time'];
      }
    }
    minTime += 0.5;
  }

  @override
  bool checkProcessing() => true;

  @override
  void processEntities(Iterable<Entity> entities) {
    elapsedTime += world.delta;
    if(entities.length == 0 && elapsedTime > minTime && !finished) {
      menu.victory(level);

      finished = true;

      assetManager.get('victory').play();
    }
  }

}