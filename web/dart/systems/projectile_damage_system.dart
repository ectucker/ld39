part of game;

class ProjectileDamageSystem extends EntityProcessingSystem {

  Mapper<Collision> collisionMapper;
  Mapper<Health> healthMapper;
  Mapper<Damage> damageMapper;
  Mapper<EntitySound> soundMapper;
  Mapper<PlayerSound> soundMapper2;

  ProjectileDamageSystem() : super(Aspect.getAspectForAllOf([Collision, Damage])) {}

  @override
  void initialize() {
    collisionMapper = new Mapper<Collision>(Collision, world);
    healthMapper = new Mapper<Health>(Health, world);
    damageMapper = new Mapper<Damage>(Damage, world);
    soundMapper = new Mapper<EntitySound>(EntitySound, world);
    soundMapper2 = new Mapper<PlayerSound>(PlayerSound, world);
  }

  @override
  void processEntity(Entity entity) {
    if(collisionMapper.has(entity)) {
      for (Entity other in collisionMapper[entity].others) {
        if (healthMapper.has(other)) {
          if (healthMapper[other].player != damageMapper[entity].player) {
            if(damageMapper[entity].beam) {
              healthMapper[other].amount -= damageMapper[entity].dmg * world.delta;
            } else {
              world.deleteEntity(entity);
              healthMapper[other].amount -= damageMapper[entity].dmg;
            }
            if(healthMapper[other].amount < 0) {
              healthMapper[other].amount = 0.0;
            }
            if(soundMapper.has(entity)) {
              soundMapper[entity].fire?.play();
            }
            if(soundMapper.has(other)) {
              soundMapper[other].damage?.play();
            }
            if(soundMapper2.has(other)) {
              soundMapper2[other].damage?.play();
            }
          }
        }
      }
    }
  }
}