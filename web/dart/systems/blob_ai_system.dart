part of game;

class BlobAISystem extends EntityProcessingSystem {

  Mapper<Position> positionMapper;
  Mapper<Input> inputMapper;
  Mapper<EntitySound> soundMapper;

  Entity player;

  BlobAISystem(this.player) : super(Aspect.getAspectForAllOf([Position, BlobAI, Input])) {}

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    inputMapper = new Mapper<Input>(Input, world);
    soundMapper = new Mapper<EntitySound>(EntitySound, world);
  }

  @override
  void processEntity(Entity entity) {
    Input input = inputMapper[entity];
    Position position = positionMapper[entity];
    Position playerPosition = positionMapper[player];

    input.moveLeft = false;
    input.moveRight = false;
    if(((playerPosition.x + 4) - (position.x + 4)).abs() > 4) {
      if(playerPosition.x < position.x) {
        input.moveLeft = true;
      } else if(playerPosition.x > position.x) {
        input.moveRight = true;
      }
    }

  }

}