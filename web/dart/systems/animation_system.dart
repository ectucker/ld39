part of game;

class AnimationSystem extends EntityProcessingSystem {

  Mapper<Animation> animationMapper;

  AnimationSystem() : super(Aspect.getAspectForAllOf([Animation])) {}

  @override
  void initialize() {
    animationMapper = new Mapper<Animation>(Animation, world);
  }

  @override
  void processEntity(Entity entity) {
    Animation animation = animationMapper[entity];

    animation.frameTime += world.delta;
    if(animation.frameTime >= 0.4) {
      animation.frameNum++;
      animation.frameTime = 0.0;
    }
    if(animation.frameNum >= animation.frames.length) {
      animation.frameNum = 0;
    }

    animation.frame = animation.frames[animation.frameNum];
  }

}