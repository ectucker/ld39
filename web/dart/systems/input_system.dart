part of game;

class InputSystem extends EntityProcessingSystem {

  Mapper<Input> inputMapper;

  InputSystem() : super(Aspect.getAspectForAllOf([PlayerInput, Input])) {}

  @override
  void initialize() {
    inputMapper = new Mapper<Input>(Input, world);
  }

  @override
  void processEntity(Entity entity) {
    Input input = inputMapper[entity];

    input.moveRight = keyboard.keyPressed(KeyCode.RIGHT);
    input.moveLeft = keyboard.keyPressed(KeyCode.LEFT);

    input.jump = keyboard.keyPressed(KeyCode.UP);

    input.castSpell1 = keyboard.keyPressed(KeyCode.Z);
    input.castSpell2 = keyboard.keyPressed(KeyCode.X);
    input.castSpell3 = keyboard.keyJustTyped(KeyCode.C);
  }

}