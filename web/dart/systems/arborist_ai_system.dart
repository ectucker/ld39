part of game;

class ArboristAISystem extends EntityProcessingSystem {

  Mapper<Position> positionMapper;
  Mapper<Input> inputMapper;
  Mapper<ArboristAI> aiMapper;
  Mapper<EntitySound> soundMapper;

  Entity player;

  ArboristAISystem(this.player) : super(Aspect.getAspectForAllOf([Position, ArboristAI, Input])) {}

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    inputMapper = new Mapper<Input>(Input, world);
    aiMapper = new Mapper<ArboristAI>(ArboristAI, world);
    soundMapper = new Mapper<EntitySound>(EntitySound, world);
  }

  @override
  void processEntity(Entity entity) {
    Input input = inputMapper[entity];
    Position position = positionMapper[entity];
    Position playerPosition = positionMapper[player];
    ArboristAI ai = aiMapper[entity];

    ai.spellCooldown -= world.delta;
    ai.vineSpawnCooldown -= world.delta;

    input.moveLeft = false;
    input.moveRight = false;
    if(ai.vinesSpawned >= 4) {
      if (((playerPosition.x + 4) - (position.x + 4)).abs() > 64) {
        if (playerPosition.x < position.x) {
          input.moveLeft = true;
        } else if (playerPosition.x > position.x) {
          input.moveRight = true;
        }
      } else if (ai.spellCooldown <= 0.0) {
        ai.vinesSpawned = 0;
        ai.spellCooldown = 7.0;
      }
    }

    if(ai.vinesSpawned < 4 && ai.vineSpawnCooldown <= 0) {
      if(playerPosition.x < position.x) {
        createVine(world, playerPosition.x + 14 - ai.vinesSpawned * 8);
      } else if(playerPosition.x > position.x) {
        createVine(world, playerPosition.x - 10 + ai.vinesSpawned * 8);
      }
      soundMapper[entity].fire.play();
      ai.vineSpawnCooldown = 0.4;
      ai.vinesSpawned++;
    }

  }

}