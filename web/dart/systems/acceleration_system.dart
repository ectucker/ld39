part of game;

class AccelerationSystem extends EntityProcessingSystem {

  Mapper<Position> positionMapper;
  Mapper<Velocity> velocityMapper;
  Mapper<Speed> speedMapper;
  Mapper<Input> inputMapper;

  AccelerationSystem() : super(Aspect.getAspectForAllOf([Position, Velocity, Speed, Input])) {}

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    inputMapper = new Mapper<Input>(Input, world);
    speedMapper = new Mapper<Speed>(Speed, world);
  }

  @override
  void processEntity(Entity entity) {
    Position position = positionMapper[entity];
    Velocity velocity = velocityMapper[entity];
    Input input = inputMapper[entity];
    Speed speed = speedMapper[entity];

    if (input.moveRight) {
      velocity.x = speed.horizantal;
    } else if (input.moveLeft) {
      velocity.x = -speed.horizantal;
    } else {
      velocity.x = 0.0;
    }

    if(input.jump && !_jumping(position)) {
      velocity.y = speed.jump;
    }
    if(position.y > 0) {
      velocity.y -= world.delta * 200;
    }

    if(position.y < 0) {
      position.y = 0.0;
      velocity.y = 0.0;
    }
  }

  bool _jumping(Position pos) {
    return pos.y > 0;
  }

}