part of game;

class FrozenManAISystem extends EntityProcessingSystem {

  Mapper<Position> positionMapper;
  Mapper<Input> inputMapper;
  Mapper<FrozenManAI> aiMapper;
  Mapper<EntitySound> soundMapper;

  Entity player;

  FrozenManAISystem(this.player) : super(Aspect.getAspectForAllOf([Position, FrozenManAI, Input])) {}

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    inputMapper = new Mapper<Input>(Input, world);
    aiMapper = new Mapper<FrozenManAI>(FrozenManAI, world);
    soundMapper = new Mapper<EntitySound>(EntitySound, world);
  }

  @override
  void processEntity(Entity entity) {
    Input input = inputMapper[entity];
    Position position = positionMapper[entity];
    Position playerPosition = positionMapper[player];
    FrozenManAI ai = aiMapper[entity];

    ai.spellCooldown -= world.delta;

    input.moveLeft = false;
    input.moveRight = false;
    if(((playerPosition.x + 4) - (position.x + 4)).abs() > 32) {
      if(playerPosition.x < position.x) {
        input.moveLeft = true;
      } else if(playerPosition.x > position.x) {
        input.moveRight = true;
      }
    } else {
      if(playerPosition.x < position.x && ai.spellCooldown <= 0) {
        createBallOfIce(world, position.x - 4, true);
        ai.spellCooldown = 1.0;
        soundMapper[entity].fire.play();
      } else if(playerPosition.x > position.x && ai.spellCooldown <= 0) {
        createBallOfIce(world, position.x + 8, false);
        ai.spellCooldown = 1.0;
        soundMapper[entity].fire.play();
      }
    }

  }

}