part of game;

class RemoveSystem extends EntityProcessingSystem {

  Mapper<RemoveAfter> removeAfterMapper;
  Mapper<RemoveOnGround> removeOnGroundMapper;
  Mapper<DestroyOutside> removeOutsideMapper;
  Mapper<RemoveWith> removeWithMapper;
  Mapper<Position> positionMapper;

  RemoveSystem() : super(Aspect.getAspectForOneOf([RemoveAfter, RemoveOnGround, DestroyOutside, RemoveWith])) {}

  @override
  void initialize() {
    removeAfterMapper = new Mapper<RemoveAfter>(RemoveAfter, world);
    removeOnGroundMapper = new Mapper<RemoveOnGround>(RemoveOnGround, world);
    positionMapper = new Mapper<Position>(Position, world);
    removeWithMapper = new Mapper<RemoveWith>(RemoveWith, world);
    removeOutsideMapper = new Mapper<DestroyOutside>(DestroyOutside, world);
  }

  @override
  void processEntity(Entity entity) {
    if(removeAfterMapper.has(entity)) {
      RemoveAfter remove = removeAfterMapper[entity];

      remove.time -= world.delta;
      if(remove.time <= 0) {
        world.deleteEntity(entity);
      }
    }

    if(removeOnGroundMapper.has(entity)) {
      Position position = positionMapper[entity];

      if(position.y <= 0.0) {
        world.deleteEntity(entity);
      }
    }

    if(removeWithMapper.has(entity)) {
      if(!removeWithMapper[entity].entity.active) {
        world.deleteEntity(entity);
      }
    }

    if(removeOutsideMapper.has(entity)) {
      Position position = positionMapper[entity];

      if(position.x <= -90 || position.x >= 90) {
        world.deleteEntity(entity);
      }
    }
  }
}