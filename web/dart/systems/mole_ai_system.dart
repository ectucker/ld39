part of game;

class MoleAISystem extends EntityProcessingSystem {

  Mapper<Position> positionMapper;
  Mapper<Input> inputMapper;
  Mapper<MoleAI> aiMapper;
  Mapper<EntitySound> soundMapper;

  Entity player;

  MoleAISystem(this.player) : super(Aspect.getAspectForAllOf([Position, MoleAI])) {}

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    inputMapper = new Mapper<Input>(Input, world);
    aiMapper = new Mapper<MoleAI>(MoleAI, world);
    soundMapper = new Mapper<EntitySound>(EntitySound, world);
  }

  @override
  void processEntity(Entity entity) {
    Position position = positionMapper[entity];
    Position playerPosition = positionMapper[player];
    MoleAI ai = aiMapper[entity];

    ai.spellCooldown -= world.delta;

    if(inputMapper.has(entity)) {
      Input input = inputMapper[entity];
      input.moveLeft = false;
      input.moveRight = false;
      if (((playerPosition.x + 4) - (position.x + 4)).abs() > 16 && ai.spellCooldown <= 2.0) {
        if (playerPosition.x < position.x) {
          input.moveLeft = true;
        } else if (playerPosition.x > position.x) {
          input.moveRight = true;
        }
      } else {
        if (playerPosition.x < position.x && ai.spellCooldown <= 0) {
          createFireBreath(world, position.x - 13, entity, true);
          ai.spellCooldown = 3.0;
        } else if (playerPosition.x > position.x && ai.spellCooldown <= 0) {
          createFireBreath(world, position.x + 9, entity, false);
          ai.spellCooldown = 3.0;
        }
        soundMapper[entity].fire.play();
      }
    } else if(position.y >= 0.0) {
      entity.addComponent(new Input());
      entity.changedInWorld();
    }
  }

}

class BombMoleAISystem extends EntityProcessingSystem {

  Mapper<Position> positionMapper;
  Mapper<Input> inputMapper;
  Mapper<BombMoleAI> aiMapper;
  Mapper<EntitySound> soundMapper;

  Entity player;

  BombMoleAISystem(this.player) : super(Aspect.getAspectForAllOf([Position, BombMoleAI])) {}

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    inputMapper = new Mapper<Input>(Input, world);
    aiMapper = new Mapper<BombMoleAI>(BombMoleAI, world);
    soundMapper = new Mapper<EntitySound>(EntitySound, world);
  }

  @override
  void processEntity(Entity entity) {
    Position position = positionMapper[entity];
    Position playerPosition = positionMapper[player];
    BombMoleAI ai = aiMapper[entity];

    if(inputMapper.has(entity)) {
      Input input = inputMapper[entity];
      input.moveLeft = false;
      input.moveRight = false;
      if (((playerPosition.x + 4) - (position.x + 4)).abs() > 10 && !ai.dropped) {
        if (playerPosition.x < position.x) {
          input.moveLeft = true;
        } else if (playerPosition.x > position.x) {
          input.moveRight = true;
        }
      } else if(ai.dropped) {
        if (playerPosition.x > position.x) {
          input.moveLeft = true;
        } else if (playerPosition.x < position.x) {
          input.moveRight = true;
        }
      } else {
        createBomb(world, position.x + 2);
        soundMapper[entity].fire.play();
        ai.dropped = true;
      }
    } else if(position.y >= 0.0) {
      entity.addComponent(new Input());
      entity.changedInWorld();
    }
  }

}