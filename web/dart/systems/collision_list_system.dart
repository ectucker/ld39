part of game;

class CollisionListSystem extends EntitySystem {

  Mapper<Hitbox> hitboxMapper;
  Mapper<Collision> collisionMapper;

  CollisionListSystem() : super(Aspect.getAspectForAllOf([Hitbox])) {}

  @override
  void initialize() {
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
    collisionMapper = new Mapper<Collision>(Collision, world);
  }

  @override
  bool checkProcessing() => true;

  @override
  void processEntities(Iterable<Entity> entities) {
    for(Entity entity in entities) {
      if(collisionMapper.has(entity)) {
        collisionMapper[entity].others.clear();
      }

      for(Entity other in entities) {
        Hitbox entityBox = hitboxMapper[entity];
        Hitbox otherBox = hitboxMapper[other];
        if(entityBox.box.intersectsWithAabb2(otherBox.box) && otherBox.box != entityBox.box) {
          if(collisionMapper.has(entity)) {
            collisionMapper[entity].others.add(other);
          } else {
            entity.addComponent(new Collision());
            collisionMapper[entity].others.add(other);
            entity.changedInWorld();
          }
        }
      }

      if(collisionMapper.has(entity) && collisionMapper[entity].others.isEmpty) {
        entity.removeComponent(Collision);
        entity.changedInWorld();
      }
    }
  }

}