part of game;

class DeathSystem extends EntityProcessingSystem {

  GameMenu menu;
  var level;
  Mapper<Health> healthMapper;

  DeathSystem(this.menu, this.level) : super(Aspect.getAspectForAllOf([Health])) {}

  @override
  void initialize() {
    healthMapper = new Mapper<Health>(Health, world);
  }

  @override
  void processEntity(Entity entity) {
    Health health = healthMapper[entity];

    if(health.amount <= 0) {
      if(!health.player) {
        world.deleteEntity(entity);
      } else if(!finished) {
        menu.death(level);
        entity.disable();
        finished = true;
        assetManager.get('death').play();
      }
    }
  }

}