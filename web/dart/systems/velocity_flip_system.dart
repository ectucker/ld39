part of game;

class VelocityFlipSystem extends EntityProcessingSystem {

  Mapper<Velocity> velocityMapper;
  Mapper<Flip> flipMapper;

  VelocityFlipSystem() : super(Aspect.getAspectForAllOf([Velocity, Flip])) {}

  @override
  void initialize() {
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    flipMapper = new Mapper<Flip>(Flip, world);
  }

  @override
  void processEntity(Entity entity) {
    Velocity velocity = velocityMapper[entity];
    Flip flip = flipMapper[entity];

    if(velocity.x < 0) {
      flip.x = true;
    }
    if(velocity.x > 0) {
      flip.x = false;
    }
  }

}