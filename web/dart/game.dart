part of game;

Map<String, Texture> atlas;

class Game extends BaseGame {

  State state, push;

  pushState(State state) {
    this.state?.pause();
    push = state;
    push.preload();
  }

  @override
  create() {
    atlas = assetManager.get('atlas');
    setGLViewport(canvasWidth, canvasHeight);
    pushState(new MainMenuState());
  }

  @override
  preload() {
    assetManager.load(
        'atlas', loadAtlas('img/atlas.json', loadTexture('img/atlas.png')));
    assetManager.load('grunt_entry', loadSound('sound/grunt_entry.ogg'));
    assetManager.load('grunt_fire', loadSound('sound/grunt_fire.ogg'));
    assetManager.load('grunt_damage', loadMusic('sound/grunt_damage.ogg'));
    assetManager.load('flower_entry', loadSound('sound/flower_entry.ogg'));
    assetManager.load('flower_fire', loadSound('sound/flower_fire.ogg'));
    assetManager.load('flower_damage', loadMusic('sound/flower_damage.ogg'));
    assetManager.load('fire_beam', loadMusic('sound/fire_beam.ogg'));
    assetManager.load('ice_shot', loadSound('sound/ice_shot.ogg'));
    assetManager.load('earth_mortar', loadSound('sound/earth_mortar.ogg'));
    assetManager.load('player_damage', loadMusic('sound/player_damage.ogg'));
    assetManager.load('arborist_entry', loadSound('sound/arborist_entry.ogg'));
    assetManager.load('arborist_fire', loadSound('sound/arborist_fire.ogg'));
    assetManager.load('arborist_damage', loadMusic('sound/arborist_damage.ogg'));
    assetManager.load('blob_entry', loadSound('sound/blob_entry.ogg'));
    assetManager.load('blob_damage', loadMusic('sound/blob_damage.ogg'));
    assetManager.load('frozenman_entry', loadSound('sound/frozenman_entry.ogg'));
    assetManager.load('frozenman_fire', loadSound('sound/frozenman_fire.ogg'));
    assetManager.load('frozenman_damage', loadMusic('sound/frozenman_damage.ogg'));
    assetManager.load('bomb_fire', loadSound('sound/bomb_fire.ogg'));
    assetManager.load('menu', loadMusic('sound/menu.ogg'));
    assetManager.load('mole_entry', loadSound('sound/mole_entry.ogg'));
    assetManager.load('mole_fire', loadMusic('sound/mole_fire.ogg'));
    assetManager.load('mole_damage', loadMusic('sound/mole_damage.ogg'));
    assetManager.load('music3', loadMusic('sound/music3.ogg'));
    assetManager.load('music2', loadMusic('sound/music2.ogg'));
    assetManager.load('music4', loadMusic('sound/music4.ogg'));
    assetManager.load('hellhound_entry', loadSound('sound/hellhound_entry.ogg'));
    assetManager.load('hellhound_fire', loadSound('sound/hellhound_fire.ogg'));
    assetManager.load('hellhound_damage', loadMusic('sound/hellhound_damage.ogg'));
    assetManager.load('lavabeast_entry', loadSound('sound/lavabeast_entry.ogg'));
    assetManager.load('lavabeast_fire', loadMusic('sound/lavabeast_fire.ogg'));
    assetManager.load('lavabeast_damage', loadMusic('sound/lavabeast_damage.ogg'));
    assetManager.load('victory', loadSound('sound/victory.ogg'));
    assetManager.load('death', loadSound('sound/death.ogg'));
  }

  @override
  update(num delta) {
    if (assetManager.allLoaded()) {
      push?.create();
      push?.resume();
      state = push ?? state;
      push = null;
    }

    state?.update(delta);
  }

  @override
  render(num delta) {
    clearScreen(Colors.black);

    state?.render(delta);
  }

  @override
  pause() {
    state?.pause();
  }

  @override
  resume() {
    state?.resume();
  }

  @override
  resize(width, height) {
    setGLViewport(canvasWidth, canvasHeight);

    state?.resize(width, height);
  }

  @override
  config() {
    scaleMode = ScaleMode.resize;
  }

}