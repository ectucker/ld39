// Copyright (c) 2017, ectucker. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library game;

import 'dart:html';
import 'package:cobblestone/cobblestone.dart';
import 'package:dartemis/dartemis.dart';
import 'package:mustache_no_mirror/mustache.dart' as Mustache;
import 'package:yaml/yaml.dart' as YAML;

part 'game.dart';
part 'game_state.dart';

part 'components/vector.dart';
part 'components/hitbox.dart';
part 'components/collision.dart';
part 'components/input.dart';
part 'components/flags.dart';
part 'components/health.dart';
part 'components/static_texture.dart';
part 'components/animation.dart';
part 'components/flip.dart';
part 'components/player_spell.dart';
part 'components/remove_after.dart';
part 'components/ai.dart';
part 'components/sound.dart';

part 'systems/movement_system.dart';
part 'systems/acceleration_system.dart';
part 'systems/hitbox_position_system.dart';
part 'systems/input_system.dart';
part 'systems/animation_system.dart';
part 'systems/render_system.dart';
part 'systems/velocity_flip_system.dart';
part 'systems/projectile_damage_system.dart';
part 'systems/collision_list_system.dart';
part 'systems/death_system.dart';
part 'systems/camera_center_system.dart';
part 'systems/player_spell_system.dart';
part 'systems/remove_system.dart';
part 'systems/grunt_ai_system.dart';
part 'systems/mole_ai_system.dart';
part 'systems/spawn_system.dart';
part 'systems/ui_update_system.dart';
part 'systems/arborist_ai_system.dart';
part 'systems/victory_system.dart';
part 'systems/blob_ai_system.dart';
part 'systems/lava_beast_ai_system.dart';
part 'systems/frozen_man_ai_system.dart';

part 'templates.dart';

part 'menu/menu.dart';
part 'menu/menu_main.dart';
part 'menu/game_menu.dart';

Game game;

void main() {
  game = new Game();
}

loadYaml(String url) async {
  return YAML.loadYaml(await HttpRequest.getString(url));
}

