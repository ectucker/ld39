part of game;

class AI extends Component {

  AI();

}

class GruntAI extends AI {

  double spellCooldown = 1.0;

  GruntAI();

}

class MoleAI extends AI {

  double spellCooldown = 1.0;

  MoleAI();

}

class FlowerAI extends AI {

  double spellCooldown = 1.0;

  FlowerAI();

}

class BombMoleAI extends AI {

  bool dropped = false;

  BombMoleAI();

}

class ArboristAI extends AI {

  double spellCooldown = 1.0;
  double vineSpawnCooldown = 0.0;
  int vinesSpawned = 10;

  ArboristAI();

}

class BlobAI extends AI {

  BlobAI();

}

class LavaBeastAI extends AI {

  double spellCooldown = 1.0;

  LavaBeastAI();

}

class FrozenManAI extends AI {

  double spellCooldown = 1.0;

  FrozenManAI();

}