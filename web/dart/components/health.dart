part of game;

class Health extends Component {

  double amount;
  bool player;

  Health(this.amount, [this.player = false]);

}

class Damage extends Component {

  double dmg;
  bool player;
  bool beam;

  Damage(this.dmg, [this.player = false, this.beam = false]);

}