part of game;

class EntitySound extends Component {

  Sound entry;
  var fire;
  Music damage;

  EntitySound(this.entry, this.fire, this.damage);

}

class PlayerSound extends Component {

  Music fireBeam;
  Sound iceShot;
  Sound earthMortar;
  Music damage;

  PlayerSound(this.fireBeam, this.iceShot, this.earthMortar, this.damage);

}