part of game;

abstract class VectorComponent extends Component {

  Vector2 vec;

  VectorComponent.fromVec(this.vec);
  VectorComponent(double x, double y): this.fromVec(new Vector2(x, y));

  double get x => vec.x;
  void set x(double x) => vec.x = x;

  double get y => vec.y;
  void set y(double y) => vec.y = y;

  void set xy(var vec) {
    x = vec.x;
    y = vec.y;
  }

  bool operator ==(other) => vec == other.vec;
  int get hashCode => vec.hashCode;

  VectorComponent clone();
  VectorComponent instanceFromVec(var vec) {
    var copy = clone();
    copy.xy = (vec);
    return copy;
  }

  VectorComponent operator +(other) {
    if (other is VectorComponent) {
      return instanceFromVec(vec + other.vec);
    }
    return instanceFromVec(vec + other);
  }
  VectorComponent operator -(other) {
    if (other is VectorComponent) {
      return instanceFromVec(vec - other.vec);
    }
    return instanceFromVec(vec - other);
  }
  VectorComponent operator -() {
    return instanceFromVec(-vec);
  }

}

class Position extends VectorComponent {

  Position.fromVec(vec): super.fromVec(vec);
  Position(x, y): super(x, y);

  @override
  clone() {
    return new Position.fromVec(vec.clone());
  }

}

class Velocity extends VectorComponent {

  Velocity.fromVec(vec): super.fromVec(vec);
  Velocity(x, y): super(x, y);

  @override
  clone() {
    return new Velocity.fromVec(vec.clone());
  }

}

class Speed extends Component {

  double horizantal;
  double jump;

  Speed(this.horizantal, [this.jump = 100.0]);

}