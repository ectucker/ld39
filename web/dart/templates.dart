part of game;

createGrunt(World world, num x) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, 0.0));
  entity.addComponent(new Animation([atlas['grunt1'], atlas['grunt2']]));
  entity.addComponent(new Velocity(0.0, 0.0));
  entity.addComponent(new Flip.none());
  entity.addComponent(new Input());
  entity.addComponent(new Hitbox(1.0, 0.0, 7.0, 12.0));
  entity.addComponent(new Health(25.0));
  entity.addComponent(new GruntAI());
  entity.addComponent(new Speed(20.0));
  entity.addComponent(new EntitySound(assetManager.get('grunt_entry'), assetManager.get('grunt_fire'), assetManager.get('grunt_damage')));
  entity.addToWorld();

  assetManager.get('grunt_entry').play();
}

createProp(World world, Texture texture, double x, double y) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, y));
  entity.addComponent(new StaticTexture(texture));
  entity.addToWorld();
}

createFireBeam(World world, double x, double y) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, y));
  entity.addComponent(new StaticTexture(atlas['firebeam']));
  entity.addComponent(new Hitbox(0.0, 0.0, 8.0, 2.0));
  entity.addComponent(new RemoveAfter(0.1));
  entity.addComponent(new Damage(13.0, true, true));
  entity.addToWorld();
}

createIceCone(World world, double x, double y, bool flip) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, y));
  entity.addComponent(new StaticTexture(atlas['icecone']));
  entity.addComponent(new Hitbox(0.0, 0.0, 8.0, 8.0));
  entity.addComponent(new RemoveAfter(5.0));
  if(flip) {
    entity.addComponent(new Velocity(-32.0, 0.0));
  } else {
    entity.addComponent(new Velocity(32.0, 0.0));
  }
  entity.addComponent(new Damage(25.0, true));
  entity.addComponent(new Flip(flip, false));
  entity.addToWorld();
}

createEarthShell(World world, double x) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, 8 * 10.0));
  entity.addComponent(new Animation([atlas['earthmissile1'], atlas['earthmissile2']]));
  entity.addComponent(new Hitbox(1.0, 0.0, 6.0, 8.0));
  entity.addComponent(new RemoveOnGround());
  entity.addComponent(new Velocity(0.0, -128.0));
  entity.addComponent(new Damage(50.0, true));
  entity.addToWorld();
}

createBallOfLight(World world, double x, bool flip) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, 8.0));
  entity.addComponent(new StaticTexture(atlas['ballolight']));
  entity.addComponent(new Hitbox(0.0, 0.0, 4.0, 4.0));
  if(flip) {
    entity.addComponent(new Velocity(-16.0, 4.0));
  } else {
    entity.addComponent(new Velocity(16.0, 4.0));
  }
  entity.addComponent(new Damage(10.0, false));
  entity.addComponent(new RemoveAfter(2.0));
  entity.addComponent(new Flip(flip, false));
  entity.addToWorld();
}

createHellMole(World world, double x) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, -16.0));
  entity.addComponent(new Animation([atlas['mole1'], atlas['mole2']]));
  entity.addComponent(new Hitbox(2.0, 0.0, 5.0, 13.0));
  entity.addComponent(new Velocity(0.0, 5.0));
  entity.addComponent(new Damage(2.0, false, true));
  entity.addComponent(new MoleAI());
  entity.addComponent(new Health(30.0));
  entity.addComponent(new Flip.none());
  entity.addComponent(new Speed(20.0));
  entity.addComponent(new EntitySound(assetManager.get('mole_entry'), assetManager.get('mole_fire'), assetManager.get('mole_damage')));
  entity.addToWorld();

  assetManager.get('mole_entry').play();
}

createFireBreath(World world, double x, Entity spawner, bool flip) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, 0.0));
  entity.addComponent(new Animation([atlas['firebreath1'], atlas['firebreath2']]));
  entity.addComponent(new Hitbox(0.0, 0.0, 13.0, 9.0));
  entity.addComponent(new Damage(20.0, false, true));
  entity.addComponent(new RemoveAfter(1.0));
  entity.addComponent(new RemoveWith(spawner));
  entity.addComponent(new Flip(flip, false));
  entity.addToWorld();
}

createArborist(World world, double x) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, 0.0));
  entity.addComponent(new Animation([atlas['arborist1'], atlas['arborist2']]));
  entity.addComponent(new Hitbox(2.0, 0.0, 5.0, 24.0));
  entity.addComponent(new Flip.none());
  entity.addComponent(new Velocity(0.0, 0.0));
  entity.addComponent(new Health(50.0));
  entity.addComponent(new ArboristAI());
  entity.addComponent(new Input());
  entity.addComponent(new Speed(20.0));
  entity.addComponent(new EntitySound(assetManager.get('arborist_entry'), assetManager.get('arborist_fire'), assetManager.get('arborist_damage')));
  assetManager.get('arborist_entry').play();
  entity.addToWorld();
}

createVine(World world, double x) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, 0.0));
  entity.addComponent(new Animation([atlas['vine1'], atlas['vine2']]));
  entity.addComponent(new Hitbox(0.0, 0.0, 5.0, 16.0));
  entity.addComponent(new Flip.none());
  entity.addComponent(new Damage(10.0, false, true));
  entity.addComponent(new RemoveAfter(2.0));
  entity.addToWorld();
}

createDropIndicator(World world, double x) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, 0.0));
  entity.addComponent(new StaticTexture(atlas['dropindicator']));
  entity.addComponent(new RemoveAfter(0.1));
  entity.addToWorld();
}

createBombMole(World world, double x) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, -16.0));
  entity.addComponent(new Animation([atlas['bomb_mole1'], atlas['bomb_mole2']]));
  entity.addComponent(new Hitbox(2.0, 0.0, 5.0, 13.0));
  entity.addComponent(new Velocity(0.0, 5.0));
  entity.addComponent(new BombMoleAI());
  entity.addComponent(new DestroyOutside());
  entity.addComponent(new Health(20.0));
  entity.addComponent(new Flip.none());
  entity.addComponent(new Speed(25.0));
  entity.addComponent(new EntitySound(assetManager.get('mole_entry'), assetManager.get('mole_fire'), assetManager.get('mole_damage')));
  entity.addToWorld();

  assetManager.get('mole_entry').play();
}

createBomb(World world, double x) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, 0.0));
  entity.addComponent(new Animation([atlas['bomb1'], atlas['bomb2']]));
  entity.addComponent(new Hitbox(0.0, 0.0, 5.0, 3.0));
  entity.addComponent(new Damage(30.0));
  entity.addComponent(new Health(5.0));
  entity.addComponent(new EntitySound(null, assetManager.get('bomb_fire'), assetManager.get('bomb_fire')));
  entity.addToWorld();
}

createFlower(World world, num x) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, 0.0));
  entity.addComponent(new Animation([atlas['flower1'], atlas['flower2']]));
  entity.addComponent(new Velocity(0.0, 0.0));
  entity.addComponent(new Flip.none());
  entity.addComponent(new Input());
  entity.addComponent(new Hitbox(1.0, 0.0, 7.0, 12.0));
  entity.addComponent(new Health(25.0));
  entity.addComponent(new FlowerAI());
  entity.addComponent(new Speed(20.0));
  entity.addComponent(new EntitySound(assetManager.get('flower_entry'), assetManager.get('flower_fire'), assetManager.get('flower_damage')));
  entity.addToWorld();

  assetManager.get('flower_entry').play();
}

createSeed(World world, double x, bool flip) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, 8.0));
  entity.addComponent(new StaticTexture(atlas['seed']));
  entity.addComponent(new Hitbox(0.0, 0.0, 4.0, 4.0));
  if(flip) {
    entity.addComponent(new Velocity(-40.0, 0.0));
  } else {
    entity.addComponent(new Velocity(40.0, 0.0));
  }
  entity.addComponent(new Flip(flip, false));
  entity.addComponent(new Damage(5.0, false));
  entity.addComponent(new RemoveAfter(3.0));
  entity.addToWorld();
}

createIceBlob(World world, double x) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, 0.0));
  entity.addComponent(new Animation([atlas['iceblob1'], atlas['iceblob2']]));
  entity.addComponent(new Velocity(0.0, 0.0));
  entity.addComponent(new Flip.none());
  entity.addComponent(new Input());
  entity.addComponent(new Hitbox(0.0, 0.0, 8.0, 5.0));
  entity.addComponent(new Health(25.0));
  entity.addComponent(new BlobAI());
  entity.addComponent(new Speed(10.0));
  entity.addComponent(new Damage(5.0, false, true));
  entity.addComponent(new EntitySound(assetManager.get('blob_entry'), null, assetManager.get('blob_damage')));
  entity.addToWorld();

  assetManager.get('blob_entry').play();
}

createHellhound(World world, double x) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, 0.0));
  entity.addComponent(new Animation([atlas['hellhound1'], atlas['hellhound2']]));
  entity.addComponent(new Velocity(0.0, 0.0));
  entity.addComponent(new Flip.none());
  entity.addComponent(new Input());
  entity.addComponent(new Hitbox(2.0, 0.0, 7.0, 5.0));
  entity.addComponent(new Health(8.0));
  entity.addComponent(new BlobAI());
  entity.addComponent(new Speed(60.0));
  entity.addComponent(new Damage(25.0));
  entity.addComponent(new EntitySound(assetManager.get('hellhound_entry'), assetManager.get('hellhound_fire'), assetManager.get('hellhound_damage')));
  entity.addToWorld();

  assetManager.get('hellhound_entry').play();
}

createLavaBeast(World world, double x) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, -24.0));
  entity.addComponent(new Animation([atlas['lavabeast1'], atlas['lavabeast2']]));
  entity.addComponent(new Velocity(0.0, 5.0));
  entity.addComponent(new Flip.none());
  entity.addComponent(new Hitbox(2.0, 0.0, 14.0, 16.0));
  entity.addComponent(new Health(90.0));
  entity.addComponent(new LavaBeastAI());
  entity.addComponent(new Speed(10.0));
  entity.addComponent(new Damage(15.0, false, true));
  entity.addComponent(new EntitySound(assetManager.get('lavabeast_entry'), assetManager.get('lavabeast_fire'), assetManager.get('lavabeast_damage')));
  entity.addToWorld();

  assetManager.get('lavabeast_entry').play();
}

createLavaSpit(World world, double x, bool flip) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, 6.0));
  entity.addComponent(new StaticTexture(atlas['lavaspit']));
  entity.addComponent(new Hitbox(0.0, 0.0, 4.0, 4.0));
  if(flip) {
    entity.addComponent(new Velocity(-24.0, -2.0));
  } else {
    entity.addComponent(new Velocity(24.0, -2.0));
  }
  entity.addComponent(new Damage(30.0, false));
  entity.addComponent(new RemoveOnGround());
  entity.addComponent(new Flip(flip, false));
  entity.addToWorld();
}

createFrozenMan(World world, double x) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, 0.0));
  entity.addComponent(new Animation([atlas['frozenman1'], atlas['frozenman2']]));
  entity.addComponent(new Hitbox(2.0, 0.0, 5.0, 24.0));
  entity.addComponent(new Flip.none());
  entity.addComponent(new Velocity(0.0, 0.0));
  entity.addComponent(new Health(50.0));
  entity.addComponent(new FrozenManAI());
  entity.addComponent(new Input());
  entity.addComponent(new Speed(20.0));
  entity.addComponent(new EntitySound(assetManager.get('frozenman_entry'), assetManager.get('frozenman_fire'), assetManager.get('frozenman_damage')));
  entity.addToWorld();

  assetManager.get('frozenman_entry').play();
}

createBallOfIce(World world, double x, bool flip) {
  Entity entity = world.createEntity();
  entity.addComponent(new Position(x, 16.0));
  entity.addComponent(new StaticTexture(atlas['balloice']));
  entity.addComponent(new Hitbox(0.0, 0.0, 4.0, 4.0));
  if(flip) {
    entity.addComponent(new Velocity(-20.0, 0.0));
  } else {
    entity.addComponent(new Velocity(20.0, 0.0));
  }
  entity.addComponent(new Damage(15.0, false));
  entity.addComponent(new RemoveAfter(1.5));
  entity.addComponent(new Flip(flip, false));
  entity.addToWorld();
}