part of game;

Camera2D camera;
bool finished;

class GameState extends MenuState {

  World world;

  String levelfile;
  var level;

  GameState(this.levelfile) : super(new GameMenu(levelfile));

  @override
  create() {
    super.create();

    finished = false;
    level = assetManager.get(levelfile);
    world = new World();

    createProp(world, atlas[level['background']], 8.0 * -9.5, -8.0);

    Entity player = world.createEntity();
    player.addComponent(new Position(0.0, 0.0));
    player.addComponent(new Velocity(0.0, 0.0));
    player.addComponent(new Hitbox(2.0, 0.0, 5.0, 24.0));
    player.addComponent(new Animation([atlas['player1'], atlas['player2']]));
    player.addComponent(new Input());
    player.addComponent(new Flip.none());
    player.addComponent(new PlayerInput());
    player.addComponent(new CameraFollow());
    player.addComponent(new PlayerSpell());
    player.addComponent(new Health(100.0, true));
    player.addComponent(new Speed(40.0));
    player.addComponent(new PlayerSound(assetManager.get('fire_beam'), assetManager.get('ice_shot'),
        assetManager.get('earth_mortar'), assetManager.get('player_damage')));
    player.addToWorld();

    world.addSystem(new SpawnSystem(level));
    world.addSystem(new InputSystem());
    world.addSystem(new GruntAISystem(player));
    world.addSystem(new BlobAISystem(player));
    world.addSystem(new FlowerAISystem(player));
    world.addSystem(new MoleAISystem(player));
    world.addSystem(new BombMoleAISystem(player));
    world.addSystem(new ArboristAISystem(player));
    world.addSystem(new FrozenManAISystem(player));
    world.addSystem(new LavaBeastAISystem(player));
    world.addSystem(new AccelerationSystem());
    world.addSystem(new MovementSystem());
    world.addSystem(new PlayerSpellSystem());
    world.addSystem(new VelocityFlipSystem());
    world.addSystem(new HitboxPositionSystem());
    world.addSystem(new CollisionListSystem());
    world.addSystem(new ProjectileDamageSystem());
    world.addSystem(new RemoveSystem());
    world.addSystem(new DeathSystem(menu, level));
    world.addSystem(new AnimationSystem());
    //world.addSystem(new CameraCenterSystem());
    world.addSystem(new RenderSystem());
    world.addSystem(new UIUpdateSystem(player, menu, level));
    world.addSystem(new VictorySystem(menu, level));

    world.initialize();

    assetManager.get(level['music']).loop();
  }

  @override
  pause() {
    super.pause();
    assetManager.get(level['music']).stop();
  }

  @override
  preload() {
    super.preload();
    assetManager.load(levelfile, loadYaml('map/' + levelfile));
  }

  @override
  render(num delta) {
    if(finished) {
      var systemsToRemove = [];
      for(var system in world.systems) {
        if(!(system is RenderSystem) && !(system is AnimationSystem)) {
          systemsToRemove.add(system);
        }
      }
      for(var system in systemsToRemove) {
        world.deleteSystem(system);
      }
    }
    clearScreen(level['background_color']['r'] / 255, level['background_color']['g'].toDouble() / 255, level['background_color']['b'].toDouble() / 255, 1.0);
    world.delta = delta;
    world.process();
  }

  @override
  resize(num width, num height) {
    RenderSystem system = world.getSystem(RenderSystem);
    system.resize(width, height);
  }

  @override
  resume() {
    super.resume();
  }

  @override
  update(num delta) {
    // TODO: implement update
  }

}